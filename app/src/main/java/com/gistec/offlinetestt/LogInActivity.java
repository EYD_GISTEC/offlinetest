package com.gistec.offlinetestt;

import android.app.Activity;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.esri.android.oauth.OAuthView;
import com.esri.core.io.OnSelfSignedCertificateListener;
import com.esri.core.io.SelfSignedCertificateHandler;
import com.esri.core.io.UserCredentials;
import com.esri.core.map.CallbackListener;

import java.security.cert.X509Certificate;

public class LogInActivity extends Activity {


    private static final int OAUTH_EXPIRATION_NEVER = -1;

    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        frameLayout = (FrameLayout)findViewById(R.id.login_layout);


        authenticationView();




    }

    OAuthView mOAuthView;

    private void authenticationView() {

        mOAuthView = new OAuthView(LogInActivity.this, getString(R.string.portal_url), getString(R.string.client_id), OAUTH_EXPIRATION_NEVER, new CallbackListener<UserCredentials>() {

            // Set a callback to inform the user if there is an error.
            @Override
            public void onError(Throwable e) {
                Log.e("Access",e.getMessage());
                // ErrorUtils.throwsError(e);
            }

            // Set a callback to continue with licensing the app once credentials
            // are entered.
            @Override
            public void onCallback(final UserCredentials credentials) {

                // Use the entered credentials to get a license.
                if (credentials != null) {

                    // can't store UserCredentials in SharedPreferences
                   // SessionManager.startLoginSession(credentials);

                    GDBUtil.setUserCredentials(credentials);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                         //   frameLayout.removeView(mOAuthView);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent;
                                    intent = new Intent(LogInActivity.this, OfflineEditorActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                        }
                    });
                    Log.e("Access","Working");

                } else {
                    Log.e("Access","failed");
                }
            }
        });

        frameLayout.addView(mOAuthView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));


        SelfSignedCertificateHandler.setOnSelfSignedCertificateListener(new OnSelfSignedCertificateListener() {
            @Override
            public boolean checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                return true;
            }
        });
        mOAuthView.setOnSslErrorListener(new OAuthView.OnSslErrorListener() {
            @Override
            public void onReceivedSslError(OAuthView oAuthView, SslErrorHandler sslErrorHandler, SslError sslError) {
                sslErrorHandler.proceed();
            }
        });



    }

}
